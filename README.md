# Bash Library

Helper programs and libraries for administration tool development (mostly in bash).

A short help is always included in the files but the full documentation contains further information with usage examples for each library. Find it under [alinex.gitlab.io/bash-lib](https://alinex.gitlab.io/bash-lib).

_Alexander Schilling_
