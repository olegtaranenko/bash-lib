#!/bin/bash
# https://github.com/kward/shunit2

source_dir=$(dirname $(readlink -f "${BASH_SOURCE[0]:-$(pwd)/x}"))
source "$source_dir/../src/include/info.bash"

test_Base_Info() {
    echo "> OS: $OS"
    assertNotNull '$OS' "$OS"
    echo "> KERNEL: $KERNEL"
    assertNotNull '$KERNEL' "$KERNEL"
    echo "> MACH: $MACH"
    assertNotNull '$MACH' "$MACH"
    echo "> DIST_BASE: $DIST_BASE"
    assertNotNull '$DIST_BASE' "$DIST_BASE"
    if [ "$DIST_BASE" = "Debian" ]; then
        echo "> DIST_BASE_REV: $DIST_BASE_REV"
        assertNotNull '$DIST_BASE_REV' "$DIST_BASE_REV"
    fi
    echo "> DIST: $DIST"
    assertNotNull '$DIST' "$DIST"
    echo "> REV: $REV"
    assertNotNull '$REV' "$REV"
    if [ "$DIST_BASE" != "Gentoo" ]; then
        echo "> REV_NAME: $REV_NAME"
        assertNotNull '$REV_NAME' "$REV_NAME"
    fi
}
test_hw_machine_id() {
    result=$(hw_machine_id)
    assertEquals "exitCode" 0 $?
    echo "> machine_id: $result"
}
test_hw_virtual() {
    result=$(hw_virtual)
    assertEquals "exitCode" 0 $?
    if [ -n "$result" ]; then
        echo "> virtual: $result"
    else
        echo "> virtual: false"
    fi
}
test_hw_cores() {
    result=$(hw_cores)
    assertEquals "exitCode" 0 $?
    echo "> cores: $result"
    assertTrue 'at least one' "[ $result -ge 1 ]"
}
test_hw_processor() {
    result=$(hw_processor)
    assertEquals "exitCode" 0 $?
    echo "> processor: $result"
    assertNotNull 'result' "$result"
}
test_hw_memory_mb() {
    result=$(hw_memory_mb)
    assertEquals "exitCode" 0 $?
    echo "> memory_mb: $result"
    assertTrue 'at least one' "[ $result -ge 100 ]"
}
test_hw_swap_mb() {
    result=$(hw_swap_mb)
    assertEquals "exitCode" 0 $?
    echo "> swap_mb: $result"
    assertTrue 'at least one' "[ $result -ge 100 ]"
}
test_hw_disks() {
    result=$(hw_disks)
    assertEquals "exitCode" 0 $?
    echo "> disks: $result" | sed ':a;N;$!ba;s/\n/\n> /g'
    assertNotNull 'result' "$result"
}
test_ip_main() {
    result=$(ip_main)
    assertEquals "exitCode" 0 $?
    echo "> ip_main: $result"
    assertNotNull 'result' "$result"
}
test_ip_list() {
    result=$(ip_list)
    assertEquals "exitCode" 0 $?
    echo "> ip_list: $result" | sed ':a;N;$!ba;s/\n/\n> ip_list: /g'
    assertNotNull 'result' "$result"
}
# skip because of rate limit on ipinfo
test_ip_info() {
    result=$(ip_info)
    assertEquals "exitCode" 0 $?
    echo "> ip_info: $result" | sed ':a;N;$!ba;s/\n/\n> ip_info: /g'
    assertNotNull 'ip' "$(echo "$result" | grep ip)"
    assertNotNull 'countr' "$(echo "$result" | grep countr)"
    assertNotNull 'region' "$(echo "$result" | grep region)"
    assertNotNull 'city' "$(echo "$result" | grep city)"
    assertNotNull 'org' "$(echo "$result" | grep org)"
}
test_package() {
    [ "$DIST_BASE" = "Linux" ] || startSkipping
    result=$(package curl)
    assertEquals "exitCode" 0 $?
    echo "> package: $result"
}

# Load shUnit2.
source_dir=$(dirname $(readlink -f "${BASH_SOURCE[0]:-$(pwd)/x}"))
source $source_dir/shunit2/shunit2
