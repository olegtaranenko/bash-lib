title: Policy

# Privacy statement

This documentation is part of the **alinex.gitlab.io** site and as such please have a look on the site's [privacy statement](/policy).
