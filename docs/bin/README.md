title: Commands

# Standalone Commands

This is a list of all included programs, which can be also called directly from bash:

-   `log` command to write to file, `STDERR` or syslog
-   `sendmail` is a simple SMTP mailer
-   `csv2html` convert CSV data into a simple HTML table
-   `csv2xls` convert CSV data into an binary XLS file
-   `install` to initially setup (see above)

## Default options

All commands supports some default options:

-   `-h --help` to show a short help integrated page
-   `-V --version` displays the version of this program

To separate parameters to the command from options a double `--` can be used.
